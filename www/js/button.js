import React, { Component } from 'react';
import './button.css';

class button extends Component {
	
 constructor(props) {
      super(props);
      this.state = {
			status: this.props.disabled ? "disabled":""
      }
		this.disabled = this.props.disabled;
		let btn_type = "default";
		if(this.props.primary){
			this.btn_type = "primary";
		}else if(this.props.secondary){
			this.btn_type = "secondary";
		}else if(this.props.flat){
			this.btn_type = "flat";
		}else{
			this.btn_type = "default";
		}
   }
   hover(){
	   if(!this.disabled){
		this.setState({status:"hover"});
	   }
   }
   disable(){
	   if(!this.disabled){
		this.setState({status:""});
	   }
   }
   render() {
    return (
		<div className={`button ${this.btn_type} ${this.state.status}`} onMouseEnter={()=>this.hover()} onMouseOut={()=>this.disable()}>{this.props.label}</div>
	);
	
  }
}

module.exports = button;