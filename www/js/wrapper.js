import React, { Component } from 'react'
import Button from './button'

class wrapper extends Component {
  constructor (props) {
    super(props)
    this.label = props.label
  }

  render () {
    
    return (
  		<div>
  		<h3>default</h3>
  		<Button label="Button"/><Button disabled label={this.label}/>
  		<h3>primary</h3>
  		<Button primary label="Button"/><Button primary disabled label={this.label}/>
  		<h3>secondary</h3>
  		<Button secondary label="Button"/><Button secondary disabled label={this.label}/>
  		<h3>flat</h3>
  		<Button flat label="Button"/><Button flat disabled label={this.label}/>
  		</div>

	   );

  }
}

export default wrapper