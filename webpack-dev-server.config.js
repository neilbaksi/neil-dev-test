const webpack = require('webpack')
const path = require('path')
const CopyWebpackPlugin = require('copy-webpack-plugin')

function excludeModule (modules) {
  return new RegExp('node_modules(?!(/|\\\\)(' + modules.join('|') + ')(/|\\\\))')
}

const config = {
  // Entry points to the project
  entry: {
    main: [
      // only- means to only hot reload for successful updates
      'webpack/hot/only-dev-server',
      'babel-polyfill',
      './www/js/app.js'
    ]
  },
  // Server Configuration options
  devServer: {
    contentBase: 'www', // Relative directory for base of server
    hot: true, // Live-reload
    inline: true,
    port: 3000, // Port Number
    historyApiFallback: true,
    host: 'localhost' // Change to '0.0.0.0' for external facing server
  },
  devtool: 'eval',
  output: {
    path: path.resolve(__dirname, 'build'), // Path of output file
    filename: 'app.js'
  },
  resolve: {
    extensions: ['.web.js', '.js', '.json', '.jsx']
  },
  node: {
    console: true,
    fs: 'empty',
    net: 'empty',
    tls: 'empty'
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('dev')
      }
    }),
    // Enables Hot Modules Replacement
    new webpack.HotModuleReplacementPlugin(),
    // Moves files
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, 'www')
      }
    ])
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [
          excludeModule(['rss-parser'])
        ],
        loader: 'babel-loader'
      },
      {
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' }
        ]
      },
      {
        test: /\.less$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'less-loader' }
        ]
      },
      {
        test: /\.svg$/,
        loader: 'file-loader'
      },
      {
        test: /\.json$/,
        loader: 'json-loader'
      }
    ]
  }
}

module.exports = config
